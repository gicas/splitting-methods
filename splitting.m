function result = splitting( ...
    F, G, t_span, iv_F, iv_G, steps_amb, ...
    coef_F, coef_G)
% SPLITTING solves a DE system, split into parts F and G.
%   SPLITTING(F, G, t_span, iv_F, iv_G, steps_amb, coef_F, coef_G)
%   solves a DE system split into F (acts of the 2nd part in this methods)
%   and G with the initial values IV_F and IV_G (vectors of 'flat' matrices),
%   on T_SPAN, using time steps that are defined by STEPS_AMB.
%   COEF_F and COEF_G are corresponding coefficient matrices or vectors.

% Dependencies
bm = blkmat;

disp(['Solving with ', mfilename, '...']);
tic

if ~isa(F, 'function_handle')
    F = @(t) F;
end
if ~isa(G, 'function_handle')
    G = @(t) G;
end

t0 = t_span(1);
tf = t_span(end);
if steps_amb > 1
    n_steps = steps_amb;
elseif steps_amb <= 1
    n_steps = (tf - t0) / steps_amb;
end
h = (tf - t0) / n_steps;

V = iv_F;
W = iv_G;

coef_F = coef_F(:);
coef_G = coef_G(:);
if size(coef_G, 1) == size(coef_F, 1) + 1
    coef_F = [zeros(1, size(coef_F, 2)); coef_F];
end

for step = 1:n_steps
    t = t0 + (step - 1) * h;
    t_a = t;
    t_b = t;
    
    for idx = 1:size(coef_F, 1)
        Ai = F(t_a);
        t_b = t_b + coef_F(idx) * h;
        Bi = G(t_b);
        t_a = t_a + coef_G(idx) * h;
        
%        if is_upper == 1
            V = V + h * coef_F(idx) * Ai * W;
            W = (h * coef_G(idx) * Bi) * V + W;
%         elseif is_upper == 0
%             W = (h * coef_f(idx) * Ai) * V + W;
%             V = V + h * coef_g(idx) * Bi * W;
%         end
    end
end
result = [V; W];

toc
% end of function
end